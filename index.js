/* application info */
global.APPVERSION_INT = 901;
global.APPVERSION = '0.9.1-RAM';
/* get selected driver name */
var rcpDriver = localStorage.getItem('rcpDriver'), cfgVersion = parseInt(
    localStorage.getItem('cfgVersion')
), drivers = {
    'mojsiuk': function (cbfn, itfn, $) {
        var options = {}, Firebird = nw.require('node-firebird'), data = {
            action: false,
            damage: false,
            worker: false
        }, console = $.console;
        options.host = localStorage.getItem('CFG_1');
        //options.host = '127.0.0.1';
        options.port = localStorage.getItem('CFG_2');
        options.database = localStorage.getItem('CFG_3');
        options.user = localStorage.getItem('CFG_4');
        options.password = localStorage.getItem('CFG_5');
        var inputLog = '', dbcPool = Firebird.pool(5, options), actionsList = false, damagesList, workersList, proc = {
            closeWorks: function (worker, day, auto, callback) {
                dbcPool.get(function (err, db) {
                    if (err) {
                        throw err;
                    }
                    var nowDate = new Date();
                    if (day == null || day == 0) {
                        day = null;
                    } else {
                        var dateTime = new Date();
                        dateTime.setDate(dateTime.getDate() + day);
                        day = dateTime.getUTCDate();
                    }
                    var src;
                    db.query(src = 'select r.ID, r.ROZPOCZECIE from "rbh_Rejestr" r ' +
                        'left outer join "rbh_Rejestr" n on n.POPRZEDNIK = r.ID where ' +
                        'r.MECHANIK = ? and (? is null or r.ROZPOCZECIE < cast(? as date)) and r.TYP = 3 and ' +
                        'n.ID is null', [worker, day, day + 1], function (err, results) {
                        console.log(src);
                        results.forEach(function (row) {
                            // console.log(row);
                            // 05.10.2009, 09:57:51.616
                            var rozpoczecie = new Date(row.ROZPOCZECIE);
                            if (auto && (row.ROZPOCZECIE + (1 / 144)) < 'NOW') {
                                rozpoczecie = row.ROZPOCZECIE + (1 / 144);
                            }
                        });
                        db.detach();
                        if (callback)
                            callback();
                    });
                });
            },
            closeDay: function (worker, auto, callback) {
            },
            openDay: function (worker, auto, callback) {
            }
        };
        this.getAction = function () {
            return data.action;
        };
        this.getActions = function (callable, workerId) {
            if (actionsList == false) {
                console.log((new Date()).getTime());
                dbcPool.get(function (err, db) {
                    if (err) throw err;
                    // pobierz typy
                    console.log((new Date()).getTime());
                    db.query('select ID, NAZWA, cast( ID as VARCHAR(10) ) as NR, STATUS_ZLECENIA from "rbh_Typy" order by ID', function (err, result) {
                        actionsList = [];
                        result.forEach(function (item) {
                            actionsList.push({
                                id: item.ID,
                                name: item.NAZWA,
                                requireDamage: item.STATUS_ZLECENIA != null,
                                raw: item
                            });
                        });
                        db.detach();
                        console.log((new Date()).getTime());
                        callable(actionsList);
                    });
                });
            } else {
                callable(actionsList);
            }
        };
        this.getDamage = function () {
            return data.damage;
        };
        this.getDamagesList = function (actionCode, workerCode, callablefn) {
            dbcPool.get(function (err, db) {
                if (err) throw err;
                var queryStr, params = [];
                if (actionCode == 3 || workerCode.length == 0) {
                    queryStr = 'select Z.ID, Z.NUMER_DLUGI, Z.UWAGI, Z.JOB, TZ.TYP as TYP_ZLEC,S.NR_NADWOZIA, S.REJESTRACJA, S.KOMIS, S.ROCZNIK, T.NAZWA as TYP from "us_Zr" Z join "us_Zlecenie" M on M.ID = Z.ZLECENIE join SAMOCHODY S on S.ID = M.POJAZD left outer join "us_PojazdyTypy" T on S.TYP_SERWISOWY = T.ID left outer join "us_ZlecenieTypy" TZ on TZ.ID = Z.TYP_ZLECENIA where M.DATA_ZAMKNIECIA is Null order by Z.NUMER_DLUGI';
                } else {
                    queryStr = 'select X.ZLECENIE, X.WAGA, X.ZR as OPERACJA from "rbh_Rejestr" X where X.ID = (select max( R.ID ) from "rbh_Rejestr" R left outer join "rbh_Rejestr" N on N.POPRZEDNIK = R.ID where R.MECHANIK = ? and R.TYP = 3 and R.ROZPOCZECIE > \'TODAY\' and N.ID is Null)';
                    params.push(workerCode);
                }
                // console.log(queryStr, params, actionCode, workerCode);
                db.query(queryStr, params, function (err, result) {
                    if (err) {
                        throw err;
                    }
                    var output = [];
                    // console.log(result);
                    result.forEach(function (item) {
                        output.push({
                            id: item.ID,
                            humanReadable: item.NUMER_DLUGI,
                            car: item.REJESTRACJA,
                            vin: item.NR_NADWOZIA,
                            description: item.UWAGI,
                            raw: item
                        });
                    });
                    db.detach();
                    damagesList = output;
                    callablefn(output);
                });
            });
        };
        this.getWorker = function () {
            return data.worker;
        };
        this.resetWorkerState = function(callable, workerId) {
            // send ping
            var execFile = nw.require('child_process').execFileSync;
            var exeFile = 'C:\\RRRe\\pbf\\php.exe';
            // exeFile = 'php';
            var output = execFile(exeFile, ['ping.php'], {
                input: JSON.stringify({
                    action: 'reset',
                    parameters: [workerId, 1],
                    connection: options
                }),
                encoding: 'utf-8'
            });
            //console.log(output);
            //console.log('select typ from "rbh_Rejestr" P where P.MECHANIK = '+ parseInt(workerId).toString() +' and ROZPOCZECIE >= \'TODAY\' order by ROZPOCZECIE DESC rows 1');
            if (output.indexOf('OK') != -1) {
                // get last command
                dbcPool.get(function(err, db){
                    if (err) throw err;
                    db.query('select typ from "rbh_Rejestr" P where P.MECHANIK = '+ parseInt(workerId).toString() +' and ROZPOCZECIE >= \'TODAY\' order by ROZPOCZECIE DESC rows 1', function(err, result){
                        //console.log(err);
                        //console.log(result);
                        if (result.length == 0) {
                            result[0] = {'TYP': 2};
                        }
                        callable(result[0].TYP);
                        db.detach();
                    });
                });
            }
        };
        this.getWorkersList = function (callablefn) {
            dbcPool.get(function (err, db) {
                if (err) throw err;
                var itemCounter = 0;
                db.query('select CAST(LIST(TYP) AS VARCHAR(4096)) AS LSTTYP, u.ID, u.NAZWA, u.STANOWISKO from UZYTKOWNICY u INNER JOIN (' +
                    'select rbhr.MECHANIK, rbhr.TYP, MAX(rbhr.DATA_REJESTRACJI) as MAXDR FROM "rbh_Rejestr" rbhr GROUP BY rbhr.MECHANIK, rbhr.TYP' +
                    ') a on (u.ID = a.MECHANIK) where ZABLOKOWANY = 0 and ZWOLNIONY = 0 GROUP BY u.ID, u.NAZWA, u.STANOWISKO', function (err, result) {
                    if (err) {
                        console.log(err);
                    }
                    var output = [];
                    result.forEach(function (el) {
                        var readyItem = {
                            id: el.ID,
                            fullName: el.NAZWA,
                            job: el.STANOWISKO,
                            raw: el,
                            lastCommand: el.LSTTYP
                        };
                        var commaPos = readyItem.lastCommand.indexOf(',');
                        if (commaPos != -1) {
                            readyItem.lastCommand = readyItem.lastCommand.substr(0, commaPos);
                        }
                        output.push(readyItem);
                    });
                    workersList = output;
                    callablefn(output);
                    if (itemCounter == 0) {
                        db.detach();
                    }
                });
            });
        };
        this.getWeightsList = function(callablefn){
            dbcPool.get(function (err, db) {
                if (err) throw err;
                // get weights from database
                db.query('SELECT a.ID, a.NAZWA FROM "us_Wagi" a', function (err, weights) {
                    callablefn(weights);
                    db.detach();
                });
            });
        };
        this.getOperationsList = function(damageId, callablefn){
            dbcPool.get(function (err, db) {
                if (err) throw err;
                console.log('select a.ID, a.NAZWA from us_ZrRozl a where a.MASTER = ?', [damageId]);
                db.query('select a.ID, a.NAZWA from "us_ZrRozl" a where a.MASTER = ?', [damageId], function (err, operations) {
                    if (err) {
                        console.log(err);
                    }
                    callablefn(operations);
                    db.detach();
                });
            });
        };
        this.sendCommand = function (callback, actionId, workerId, damageId, operationId, weightId) {
            dbcPool.get(function (err, db) {
                if (err) throw err;
                var params = [actionId, workerId, damageId, operationId, weightId];
                for (var otr = 0; otr < 5; otr++) {
                    if (isNaN(params[otr]) || params[otr].length < 1) {
                        params[otr] = 'NULL';
                    }
                }
                console.log(params);
                params = '--command=' + params.join('_');
                var execFile = nw.require('child_process').execFileSync;
                var exeFile = 'C:\\RRRe\\pbf\\php.exe';
                // exeFile = 'php';
                var output = execFile(exeFile, ['ping.php'], {
                    input: JSON.stringify({
                        action: 'ping',
                        parameters: [actionId, workerId, damageId, operationId, weightId],
                        connection: options
                    }),
                    encoding: 'utf-8'
                });
                console.log(output);
                if (output.indexOf('OK') != -1) {
                    // send notification to dealerbms
                    var kod = [parseInt(actionId)];
                    if (kod[0] == 3) {
                        kod.push(parseInt(weightId));
                    }
                    var string = JSON.stringify({
                        RCP_ZLECDOC_ID: damageId,
                        RCP_USER_ID: workerId,
                        EVENT_TIME: Math.floor(Date.now() / 1000),
                        EVENT_KOD: JSON.stringify(kod)
                    });
                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', 'http://dev.dealerbms.pl/restcs/command', true);
                    xhr.setRequestHeader('application-id', localStorage.getItem('ApplicationID'));
                    xhr.setRequestHeader('rcp-proto-ver', '2');
                    xhr.onreadystatechange = function () {
                        switch (xhr.readyState) {
                            case 4:
                                if (xhr.status !== 200) {
                                    callback({
                                        success: false,
                                        message: 'Błąd dodawania wpisu (API)'
                                    });
                                    return;
                                }
                                callback({
                                    success: true,
                                    message: 'Dodano wpis'
                                });
                                console.log(xhr.responseText);
                                data = {
                                    action: false,
                                    damage: false,
                                    worker: false
                                };
                                break;
                            default:
                                return;
                        }
                    };
                    xhr.send(string);
                } else {
                    callback({
                        success: false,
                        message: 'Błąd dodawania wpisu (RCP)'
                    });
                }
            });
        };
        this.setAction = function (actionId) {
            var action = actionsList.filter(function (item) {
                return item.id == actionId;
            });
            data.action = (action.length > 0) ? action.pop() : false;
            cbfn();
        };
        this.setDamage = function (damageId) {
            data.damage = false;
            var damage = damagesList.filter(function (item) {
                return item.id == damageId;
            });
            data.damage = (damage.length > 0) ? damage.pop() : false;
            cbfn();
        };
        this.setWorker = function (userId) {
            data.worker = false;
            var worker = workersList.filter(function (item) {
                return item.id == userId;
            });
            data.worker = (worker.length > 0) ? worker.pop() : false;
            cbfn();
        };
        this.ResetApp = function () {
            data = {
                action: false,
                damage: false,
                worker: false
            };
            cbfn();
        };
        this.ReadInput = function (e) {
            if (e.keyCode == 13) {
                // enter pressed (or finished input by reader)
                return;
            }
            inputLog += (String.fromCharCode(e.keyCode));
            console.log(inputLog);
            cbfn();
        };
        /**
         * @return {boolean}
         */
        this.AllowSearch = function () {
            return !0;
        };
        itfn(this);
    }
};
if (global.APPVERSION_INT <= cfgVersion && drivers.hasOwnProperty(rcpDriver)) {
    global.Protocol = drivers[rcpDriver];
    nw.Window.open('static/interface2.html', {
        height: 600,
        width: 1000
    }, function (win) {
    });
} else {
    localStorage.setItem('rcpDriver', 'mojsiuk');
    nw.Window.open('static/configuration.html', {
        height: 600,
        width: 1000
    }, function (win) {
    });
}